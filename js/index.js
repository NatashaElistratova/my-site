var about = document.getElementById("about");
var resume = document.getElementById("resume");
var projects = document.getElementById("projects");
var contacts = document.getElementById("contacts");
var contentWrapper = document.getElementById('contentWrapper');
var menuList = document.getElementsByClassName('sidebar')[0];
var nav = document.getElementsByTagName('nav')[0];
var title = document.getElementsByClassName("name")[0];
var prof = document.getElementsByClassName("prof")[0];
var phone1 = document.getElementsByClassName("phone")[0];
// var phone2 = document.getElementsByClassName("phone")[1];
// var email = document.getElementsByClassName("email")[0];

var fadeInText = document.getElementById("fadeInText");
var body = document.getElementsByTagName("body")[0];
var footer = document.getElementsByTagName("footer")[0];
var date = new Date();
var currentYear = document.getElementById("year");
var selectedLi;

// click sidebar
function showContent() {
  this.classList.toggle('clickMenu');
  contentWrapper.style.right = '-800px';

  var request = new XMLHttpRequest();

  if (selectedLi) {
    selectedLi.classList.remove('clickMenu');
  }

  if (this === about && this !== selectedLi){
      history.pushState(null, null, '#about');
      request.open('GET', '/content/about.html', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send();

      setTimeout(function() {
        contentWrapper.innerHTML = request.response;
        contentWrapper.style.right = '10%';
        contentWrapper.style.borderLeft = '8px solid #cc0066';
      }, 800);
  };

  if (this === resume && this !== selectedLi){
      history.pushState(null, null, '#resume');
      request.open('GET', '/content/resume.html', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send();

      setTimeout(function() {
        contentWrapper.innerHTML = request.response;
        contentWrapper.style.right = '10%';
        contentWrapper.style.borderLeft = '8px solid #00cc44';
      }, 800);


  };

  if (this === projects && this !== selectedLi){
    history.pushState(null, null, '#projects');
      request.open('GET', '/content/projects.html', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send();

      setTimeout(function() {
        contentWrapper.innerHTML = request.response;
        contentWrapper.style.right = '10%';
        contentWrapper.style.borderLeft = '8px solid #FFFF00';

        var projectsWrapper = document.getElementById("projectsWrapper");
        projectsWrapper.addEventListener("click",showImg);
      }, 800);

      //projects img click
      function showImg(event){
        var target = event.target;
        if (target.tagName != 'IMG') return;

        var coverDiv = document.createElement('div');
            coverDiv.id = 'cover-div';
            document.body.appendChild(coverDiv);

        var src = target.src;
        var innerImg =  document.createElement('img');
            coverDiv.appendChild(innerImg);
            innerImg.src = src;
            innerImg.style.width = "70%";

        var closeLink = document.createElement('a');
            closeLink.classList.add('close');
            coverDiv.appendChild(closeLink);
            closeLink.textContent = "X";

        closeLink.addEventListener("click",function(){
          document.body.removeChild(coverDiv);
        });
      };
      //END projects img click
  };

  if (this === contacts && this !== selectedLi){
      request.open('GET', '/content/contacts.html', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send();

      setTimeout(function() {
        contentWrapper.innerHTML = request.response;
        contentWrapper.style.right = '10%';
        contentWrapper.style.borderLeft = '8px solid #ff3300';
      }, 800);
  };

selectedLi = this;
};

about.addEventListener("click",showContent);

resume.addEventListener("click",showContent);

projects.addEventListener("click",showContent);

contacts.addEventListener("click",showContent);
// END click sidebar


// click title
  function hideContent() {
    event.stopPropagation();
     contentWrapper.style.right = '-800px';
     nav.classList.toggle('showNav');
     selectedLi.classList.remove('clickMenu');
  }

  title.addEventListener("click",hideContent);
// END click title

//page start
function startPage(){
  nav.classList.add('showNav');
  fadeInText.style.display = "none";
  
  setTimeout(function(){
	 footer.style.display = "block"; 
  },1500);
  
}

body.addEventListener("click", startPage);
//END page start


//footer date
var year = date.getFullYear();
currentYear.innerHTML = year;
//END footer date

// // Fade In text
function fadeIn(el) {
  el.style.opacity = 0;

  var last = +new Date();
  var tick = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
    last = +new Date();

    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };

  tick();
}
// // END Fade In text
setTimeout(function() {
  fadeIn(prof);
  fadeIn(phone1);
}, 3500);

// setTimeout(function() {
//   fadeIn(phone1);
// }, 5500);
//
// setTimeout(function() {
//   fadeIn(phone2);
// }, 6000);
//
// setTimeout(function() {
//   fadeIn(email);
// }, 6500);
